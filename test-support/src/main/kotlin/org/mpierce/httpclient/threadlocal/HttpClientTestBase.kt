package org.mpierce.httpclient.threadlocal

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.ContentNegotiation
import io.ktor.http.ContentType
import io.ktor.jackson.JacksonConverter
import io.ktor.response.respond
import io.ktor.routing.Routing
import io.ktor.routing.get
import io.ktor.server.engine.ApplicationEngine
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

abstract class HttpClientTestBase(val port: Int) {
    private lateinit var server: ApplicationEngine

    protected val headerName = "x-call-id"

    abstract fun callHeadersEndpoint(): Map<String, List<String>>
    abstract fun defaultHeadersProvidedByClient(): Map<String, List<String>>

    protected val tl = ThreadLocal.withInitial<String?> { null }!!

    @BeforeEach
    internal fun setUpServer() {
        server = embeddedServer(Netty, port = port) {
            install(ContentNegotiation) {
                register(
                    ContentType.Application.Json,
                    JacksonConverter(mapper)
                )
            }
            install(Routing) {
                get("/headers") {
                    call.respond(
                        call.request.headers
                            .entries()
                            .map { Pair(it.key, it.value) }
                            .toMap()
                    )
                }
            }
        }
        server.start(wait = false)
        tl.remove()
    }

    @AfterEach
    internal fun tearDownServer() {
        server.stop(10, 10)
    }

    @Test
    internal fun noHeaderWhenNoContext() {
        val reqHeaders = callHeadersEndpoint()
        assertEquals(defaultHeadersProvidedByClient(), reqHeaders)
    }

    @Test
    internal fun sendsHeaderWhenContextPresent() {
        tl.set("foo")
        val reqHeaders = callHeadersEndpoint()
        assertEquals(
            defaultHeadersProvidedByClient()
                .plus(headerName to listOf("foo")),
            reqHeaders
        )
    }
}

val mapper = ObjectMapper().apply {
    registerModule(KotlinModule())
}
