val deps: Map<String, String> by extra

dependencies {
    api(project(":core"))

    implementation(kotlin("stdlib"))

    implementation("io.ktor:ktor-server-core:${deps["ktor"]}")
    implementation("io.ktor:ktor-server-netty:${deps["ktor"]}")
    implementation("io.ktor:ktor-jackson:${deps["ktor"]}")
    api("com.fasterxml.jackson.core:jackson-databind:2.12.2")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.12.2")
    runtimeOnly("org.slf4j:slf4j-simple:1.7.30")

    implementation("org.junit.jupiter:junit-jupiter-api:${deps["junit"]}")
    runtimeOnly("org.junit.jupiter:junit-jupiter-engine:${deps["junit"]}")
}
