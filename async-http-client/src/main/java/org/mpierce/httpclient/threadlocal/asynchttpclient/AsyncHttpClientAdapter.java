package org.mpierce.httpclient.threadlocal.asynchttpclient;

import org.asynchttpclient.filter.FilterContext;
import org.asynchttpclient.filter.FilterException;
import org.asynchttpclient.filter.RequestFilter;
import org.mpierce.httpclient.threadlocal.core.HeaderModifier;
import org.mpierce.httpclient.threadlocal.core.RequestHeaders;

/**
 * Adapter for using a {@link HeaderModifier} with Async Http Client.
 *
 * Add these as request filters when building the client instance:
 *
 * <pre>
HeaderModifier h = ...;
AsyncHttpClient client = Dsl.config().addRequestFilter(new AsyncHttpClientAdapter(h)).build();
 * </pre>
 */
public class AsyncHttpClientAdapter implements RequestFilter {

    private final HeaderModifier headerModifier;

    public AsyncHttpClientAdapter(HeaderModifier headerModifier) {
        this.headerModifier = headerModifier;
    }

    @Override
    public <T> FilterContext<T> filter(FilterContext<T> ctx) throws FilterException {
        headerModifier.apply(new AsyncHttpClientRequestHeaders<>(ctx));

        return ctx;
    }

    private static class AsyncHttpClientRequestHeaders<T> implements RequestHeaders {
        private final FilterContext<T> filterContext;

        private AsyncHttpClientRequestHeaders(FilterContext<T> filterContext) {
            this.filterContext = filterContext;
        }

        @Override
        public void addHeader(String name, String value) {
            filterContext.getRequest().getHeaders().add(name, value);
        }
    }
}
