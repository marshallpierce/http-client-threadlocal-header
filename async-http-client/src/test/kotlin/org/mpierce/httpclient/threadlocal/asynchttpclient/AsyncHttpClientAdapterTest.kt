package org.mpierce.httpclient.threadlocal.asynchttpclient

import com.fasterxml.jackson.core.type.TypeReference
import org.asynchttpclient.Dsl.asyncHttpClient
import org.asynchttpclient.Dsl.config
import org.junit.jupiter.api.AfterEach
import org.mpierce.httpclient.threadlocal.HttpClientTestBase
import org.mpierce.httpclient.threadlocal.core.SingleHeaderAdder
import org.mpierce.httpclient.threadlocal.mapper
import java.util.function.Function

internal class AsyncHttpClientAdapterTest : HttpClientTestBase(12000) {
    override fun callHeadersEndpoint(): Map<String, List<String>> {
        val resp = client.prepareGet(("http://localhost:$port/headers")).execute().get()
        return resp.responseBodyAsStream.let {
            mapper.reader()
                .forType(object : TypeReference<Map<String, List<String>>>() {})
                .readValue(it)
        }
    }

    override fun defaultHeadersProvidedByClient(): Map<String, List<String>> = mapOf(
        "host" to listOf("localhost:$port"),
        "accept" to listOf("*/*"),
        "user-agent" to listOf("AHC/2.1")
    )

    private val client = config()
        .addRequestFilter(
            AsyncHttpClientAdapter(
                SingleHeaderAdder(headerName, tl, Function.identity())
            )
        )
        .build()
        .let { asyncHttpClient(it) }

    @AfterEach
    internal fun tearDownClient() {
        client.close()
    }
}
