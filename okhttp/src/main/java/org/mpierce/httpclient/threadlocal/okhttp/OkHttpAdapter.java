package org.mpierce.httpclient.threadlocal.okhttp;

import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import org.jetbrains.annotations.NotNull;
import org.mpierce.httpclient.threadlocal.core.HeaderModifier;
import org.mpierce.httpclient.threadlocal.core.RequestHeaders;

/**
 * Adapter for using a {@link HeaderModifier} with OkHttp.
 *
 * Add these as network interceptors when building the client instance:
 *
 * <pre>
HeaderModifier h = ...;
OkHttpClient c = OkHttpClient.Builder()
    .addNetworkInterceptor(OkHttpAdapter(h))
    .build();
 * </pre>
 */
public class OkHttpAdapter implements Interceptor {

    private final HeaderModifier headerModifier;

    public OkHttpAdapter(HeaderModifier headerModifier) {
        this.headerModifier = headerModifier;
    }

    @NotNull
    @Override
    public Response intercept(@NotNull Chain chain) throws IOException {
        Request origReq = chain.request();

        Request.Builder builder = origReq.newBuilder();

        headerModifier.apply(new OkHttpRequest(builder));

        return chain.proceed(builder.build());
    }

    private static class OkHttpRequest implements RequestHeaders {

        private final Request.Builder builder;

        private OkHttpRequest(Request.Builder builder) {
            this.builder = builder;
        }

        @Override
        public void addHeader(String name, String value) {
            builder.addHeader(name, value);
        }
    }
}
