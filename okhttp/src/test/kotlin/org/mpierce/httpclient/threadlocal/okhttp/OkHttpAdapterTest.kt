package org.mpierce.httpclient.threadlocal.okhttp

import com.fasterxml.jackson.core.type.TypeReference
import okhttp3.OkHttpClient
import okhttp3.Request
import org.mpierce.httpclient.threadlocal.HttpClientTestBase
import org.mpierce.httpclient.threadlocal.core.SingleHeaderAdder
import org.mpierce.httpclient.threadlocal.mapper
import java.util.function.Function

internal class OkHttpAdapterTest : HttpClientTestBase(12002) {
    override fun callHeadersEndpoint(): Map<String, List<String>> {
        val resp = client.newCall(Request.Builder().url("http://localhost:$port/headers").build()).execute()
        return resp.body.use {
            mapper.reader()
                .forType(object : TypeReference<Map<String, List<String>>>() {})
                .readValue(it!!.byteStream())
        }
    }

    override fun defaultHeadersProvidedByClient(): Map<String, List<String>> = mapOf(
        "Host" to listOf("localhost:$port"),
        "Connection" to listOf("Keep-Alive"),
        "Accept-Encoding" to listOf("gzip"),
        "User-Agent" to listOf("okhttp/4.9.1")
    )

    private val client = OkHttpClient.Builder()
        .addNetworkInterceptor(OkHttpAdapter(SingleHeaderAdder(headerName, tl, Function.identity())))
        .build()
}
