 [ ![Download](https://api.bintray.com/packages/marshallpierce/maven/http-client-threadlocal-header/images/download.svg) ](https://bintray.com/marshallpierce/maven/http-client-threadlocal-header/_latestVersion) 

This library provides integrations with various JVM http clients to set request headers with data from a `ThreadLocal`.

# Quick start

```java
class Demo {
    public static void main(String[] args){
        // a ThreadLocal you'd like to use for request id
        // When its contents are non-null for a given thread, the contents will be
        // included as the value of a request header `x-request-id` on any outbound
        // requests.        
        var currentRequestId = new ThreadLocal<>();
        
        // Set up a HeaderModifier to add a single header based on the ThreadLocal
        // In this case, we have a ThreadLocal<String> so we don't need to transform it
        // to get a String to put into a header, so we use the identity function.
        var headerAdder = new SingleHeaderAdder("x-request-id", currentRequestId, 
                Function.identity());
        
        // Use that with as many http clients as you like. 
        // Here, we use Async Http Client with the corresponding adapter:
        var ahc = Dsl.asyncHttpClient(Dsl.config()
                .addRequestFilter(new AsyncHttpClientAdapter(headerAdder))
                .build());  
        
        // Or OkHttp with a different adapter
        var okhttp = OkHttpClient.Builder()
                 .addNetworkInterceptor(new OkHttpAdapter(headerAdder))
                 .build();

        // etc, for other http clients

        // It's now up to you to set the thread local as you see fit, presumably by
        // intercepting inbound HTTP requests and setting the threadlocal if the request
        // id header is found.
    }
}
```

# Why?

It's fairly common in a complex system to need to pass diagnostic information between services, like a unique request ID that could be used to filter log output across multiple services for all work related to one particular user request. Suppose you have a system that looks like this:

```
 request     v------------------(Service A)------------------v    request to svc B
 ----------> [ (http server) -> (app logic) -> (http client) ] ------------------>
```

Here, Service A accepts HTTP requests, performs some logic, and makes further HTTP requests to some other service. If the initial inbound request has the header `x-request-id`, it would be nice to pass that along to the request to service B as well so that you could search by request id `6540CDBD-9F3F-4199-AE48-94F19F574BE8` in the logs across all services.

# Dependencies

There are various subprojects: `core` for the, well, core types, and one per each http client for the necessary adapter to that client's particular way of customizing http requests (`async-http-client`, `okhttp`, `jetty-client`, etc). Add dependencies to your project structureo for each http client you use.

# Thread local limitations

Thread locals do not inherit if you start a thread, and do not carry over to tasks you give to a thread pool. 

Suppose you are serving HTTP endpoints via servlets, JAX-RS, etc where one thread is dedicated to handling an individual request from start to finish (your app logic, any sort of pre or post filtering or intercepting, etc). Further suppose that you have added a filter/interceptor/whatever that looks for a particular header and, if present, sets the value of that header in a thread local. 

If you read that thread local from on that thread (say, by making an HTTP request with a client set up to use this library's features), you will get the contents of the header (and the outbound HTTP request will have the header added).

If, on the other hand, you delegate some work to another thread, and the logic running on that other thread starts an HTTP request, it will _not_ get the header value from the thread local because it's a different thread with its own storage for that thread local.

If you're in a position to use [Kotlin coroutines](https://kotlinlang.org/docs/reference/coroutines-overview.html), the coroutine context has a much nicer solution for this in two ways. First, coroutine context is inherited when you spawn new coroutines, and second, [asContextElement](https://kotlin.github.io/kotlinx.coroutines/kotlinx-coroutines-core/kotlinx.coroutines/java.lang.-thread-local/as-context-element.html) can map values set in coroutine context (like, say, the current request id) into thread locals so that the thread the coroutine is running on will have the appropriate thread local set. This means that if there's a coroutine (rather than thread as above) per request, you could set the request id in the coroutine context, and also use a threadlocal with `asContextElement` so that this library can use it, and then even if you spawn a child coroutine and run that coroutine in another thread, everything will Just Work™.

To see an example of a coroutine-based approach, check out [ktor-threadlocal-coroutine-context](https://bitbucket.org/marshallpierce/ktor-threadlocal-coroutine-context/src/master/), which makes it easy to set a thread local in the coroutine context Ktor creates for each request.
