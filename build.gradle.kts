import java.net.URI
import java.time.Duration
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.4.32" apply false
    id("com.github.ben-manes.versions") version "0.38.0"
    id("io.github.gradle-nexus.publish-plugin") version "1.0.0"
    id("net.researchgate.release") version "2.8.1"
    id("org.jmailen.kotlinter") version "3.4.0"
}

subprojects {
    apply(plugin = "org.jetbrains.kotlin.jvm")
    apply(plugin = "org.jmailen.kotlinter")

    group = "org.mpierce.http.client.threadlocalheader"

    repositories {
        mavenCentral()
    }

    @Suppress("UNUSED_VARIABLE")
    val deps by extra {
        mapOf(
                "ahc" to "2.12.3",
                "jetty" to "9.4.39.v20210325",
                "okhttp" to "4.9.1",
                "junit" to "5.7.1",
                "ktor" to "1.5.2"
        )
    }

    configure<JavaPluginConvention> {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    tasks.named<Test>("test") {
        useJUnitPlatform()
    }

    tasks.withType<KotlinCompile> {
        kotlinOptions.jvmTarget = "1.8"
    }
}


subprojects.filter { !listOf("test-support").contains(it.name) }.forEach { project ->
    project.run {
        apply(plugin = "maven-publish")
        apply(plugin = "signing")

        tasks {
            register<Jar>("docJar") {
                from(project.tasks["javadoc"])
                archiveClassifier.set("javadoc")
            }
        }

        configure<JavaPluginExtension> {
            withSourcesJar()
        }

        configure<PublishingExtension> {
            publications {
                register<MavenPublication>("sonatype") {
                    from(components["java"])
                    artifact(tasks["docJar"])
                    // sonatype required pom elements
                    pom {
                        name.set("${project.group}:${project.name}")
                        description.set(name)
                        url.set("https://bitbucket.org/marshallpierce/http-client-threadlocal-header")
                        licenses {
                            license {
                                name.set("Copyfree Open Innovation License 0.5")
                                url.set("https://copyfree.org/content/standard/licenses/coil/license.txt")
                            }
                        }
                        developers {
                            developer {
                                id.set("marshallpierce")
                                name.set("Marshall Pierce")
                                email.set("575695+marshallpierce@users.noreply.github.com")
                            }
                        }
                        scm {
                            connection.set("scm:git:https://bitbucket.org/marshallpierce/http-client-threadlocal-header")
                            developerConnection.set("scm:git:ssh://git@bitbucket.org:marshallpierce/http-client-threadlocal-header.git")
                            url.set("https://bitbucket.org/marshallpierce/http-client-threadlocal-header")
                        }
                    }
                }
            }

            // A safe throw-away place to publish to:
            // ./gradlew publishSonatypePublicationToLocalDebugRepository -Pversion=foo
            repositories {
                maven {
                    name = "localDebug"
                    url = URI.create("file:///${project.buildDir}/repos/localDebug")
                }
            }
        }

        // don't barf for devs without signing set up
        if (project.hasProperty("signing.keyId")) {
            configure<SigningExtension> {
                sign(project.extensions.getByType<PublishingExtension>().publications["sonatype"])
            }
        }

        // releasing should publish
        rootProject.tasks.afterReleaseBuild {
            dependsOn(provider { project.tasks.named("publishToSonatype") })
        }
    }
}

nexusPublishing {
    repositories {
        sonatype {
            // sonatypeUsername and sonatypePassword properties are used automatically
            stagingProfileId.set("ab8c5618978d18") // org.mpierce
        }
    }
    // these are not strictly required. The default timeouts are set to 1 minute. But Sonatype can be really slow.
    // If you get the error "java.net.SocketTimeoutException: timeout", these lines will help.
    connectTimeout.set(Duration.ofMinutes(3))
    clientTimeout.set(Duration.ofMinutes(3))
}

tasks {
    // dummy for release plugin
    register("build") {}
}
