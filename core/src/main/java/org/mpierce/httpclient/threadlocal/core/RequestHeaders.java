package org.mpierce.httpclient.threadlocal.core;

/**
 * An abstraction around client-specific ways to add request headers.
 */
public interface RequestHeaders {
    /**
     * Adds a header to the underlying request.
     *
     * @param name  header name
     * @param value header value
     */
    void addHeader(String name, String value);
}
