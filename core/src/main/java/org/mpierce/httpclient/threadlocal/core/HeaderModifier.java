package org.mpierce.httpclient.threadlocal.core;

/**
 * A callback to modify request headers, invoked when a request is about to be sent.
 *
 * <p>
 * To use an instance with a client, pick the appropriate adapter for the client in question and wrap this with that
 * adapter, e.g. {@code new AsyncHttpClientAdapter(headerModifier)}. Registering the adapter with the client is
 * client-specific; see the docs for each adapter.
 * </p>
 *
 * <p>
 * Probably users will just use {@link SingleHeaderAdder}, but it is reasonable to write your own implementation if
 * setting one header at a time from a ThreadLocal is not ideal.
 * </p>
 *
 * <p>
 * Implementations should be thread safe if they're used with http clients that are shared across threads.
 * </p>
 */
public interface HeaderModifier {
    /**
     * Apply implementation-specific modifications to the request headers. Invoked from the same thread that initially
     * kicked off the HTTP request (i.e. app logic, not some client-controlled background processing thread).
     *
     * @param headers the headers for the request
     */
    void apply(RequestHeaders headers);
}
