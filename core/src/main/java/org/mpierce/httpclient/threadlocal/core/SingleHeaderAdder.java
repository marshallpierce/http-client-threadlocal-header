package org.mpierce.httpclient.threadlocal.core;

import java.util.function.Function;

/**
 * Sets a single header based on the contents of a ThreadLocal.
 *
 * @param <T> The type contained in the ThreadLocal
 */
public class SingleHeaderAdder<T> implements HeaderModifier {
    private final String headerName;
    private final ThreadLocal<T> threadLocal;
    private final Function<T, String> function;

    /**
     * @param headerName  the header to set
     * @param threadLocal the ThreadLocal to access
     * @param function    the function to map the ThreadLocal to a String. If the function evaluates to null, no header
     *                    will be set.
     */ 
    public SingleHeaderAdder(String headerName, ThreadLocal<T> threadLocal,
            Function<T, String> function) {
        this.headerName = headerName;
        this.threadLocal = threadLocal;
        this.function = function;
    }

    @Override
    public void apply(RequestHeaders headers) {
        String result = function.apply(threadLocal.get());

        if (result != null) {
            headers.addHeader(headerName, result);
        }
    }
}
