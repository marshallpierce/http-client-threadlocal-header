val deps: Map<String, String> by extra

dependencies {
    api(project(":core"))
    api("org.eclipse.jetty:jetty-client:${deps["jetty"]}")

    testImplementation(project(":test-support"))
    testImplementation(kotlin("stdlib"))

    testImplementation("org.junit.jupiter:junit-jupiter-api:${deps["junit"]}")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:${deps["junit"]}")
}
