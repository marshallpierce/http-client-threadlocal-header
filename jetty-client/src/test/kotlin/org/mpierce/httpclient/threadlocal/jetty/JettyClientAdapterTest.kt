package org.mpierce.httpclient.threadlocal.jetty

import com.fasterxml.jackson.core.type.TypeReference
import org.eclipse.jetty.client.HttpClient
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.mpierce.httpclient.threadlocal.HttpClientTestBase
import org.mpierce.httpclient.threadlocal.core.SingleHeaderAdder
import org.mpierce.httpclient.threadlocal.mapper
import java.util.function.Function

internal class JettyClientAdapterTest : HttpClientTestBase(12001) {
    override fun callHeadersEndpoint(): Map<String, List<String>> {
        val resp = client.GET("http://localhost:$port/headers")
        return resp.contentAsString.let {
            mapper.reader()
                .forType(object : TypeReference<Map<String, List<String>>>() {})
                .readValue(it)
        }
    }

    override fun defaultHeadersProvidedByClient(): Map<String, List<String>> = mapOf(
        "Accept-Encoding" to listOf("gzip"),
        "User-Agent" to listOf("Jetty/9.4.39.v20210325"),
        "Host" to listOf("localhost:$port")
    )

    private val client = HttpClient().apply {
        requestListeners.add(JettyClientAdapter(SingleHeaderAdder(headerName, tl, Function.identity())))
    }

    @BeforeEach
    internal fun setUpClient() {
        client.start()
    }

    @AfterEach
    internal fun tearDownClient() {
        client.stop()
    }
}
