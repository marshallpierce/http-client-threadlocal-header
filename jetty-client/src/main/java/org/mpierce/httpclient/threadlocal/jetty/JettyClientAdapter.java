package org.mpierce.httpclient.threadlocal.jetty;

import java.nio.ByteBuffer;
import org.eclipse.jetty.client.api.Request;
import org.mpierce.httpclient.threadlocal.core.HeaderModifier;
import org.mpierce.httpclient.threadlocal.core.RequestHeaders;

/**
 * Adapter for using a {@link HeaderModifier} with Jetty Client.
 *
 * Add these as request listeners when building the client instance:
 *
 * <pre>
HeaderModifier h = ...;
HttpClient c = new HttpClient();
c.getRequestListeners().add(new JettyClientAdapter(h)).build();
 * </pre>
 */
public class JettyClientAdapter implements Request.Listener {
    private final HeaderModifier headerModifier;

    public JettyClientAdapter(HeaderModifier headerModifier) {
        this.headerModifier = headerModifier;
    }

    @Override
    public void onBegin(Request request) {
        // no op
    }

    @Override
    public void onCommit(Request request) {
        // no op
    }

    @Override
    public void onContent(Request request, ByteBuffer content) {
        // no op
    }

    @Override
    public void onFailure(Request request, Throwable failure) {
        // no op
    }

    @Override
    public void onHeaders(Request request) {
        // no op
    }

    @Override
    public void onQueued(Request request) {
        // onQueued is called on the originating thread, allowing us to grab threadlocals
        headerModifier.apply(new JettyHeaders(request));
    }

    @Override
    public void onSuccess(Request request) {
        // no op
    }

    private static class JettyHeaders implements RequestHeaders {

        private final Request request;

        private JettyHeaders(Request request) {
            this.request = request;
        }

        @Override
        public void addHeader(String name, String value) {
            request.header(name, value);
        }
    }
}
